import React from "react";
import { render, fireEvent } from "@testing-library/react";
import {
  Button,
  ChatBalloon,
  OnlineUser,
  TextInput,
} from "@/app/_components/atoms";
import "@testing-library/jest-dom";

describe("Button", () => {
  it("renders the correct text", () => {
    const { getByText } = render(
      <Button onClick={() => {}} text="Test Button" color="bg-red-500" />
    );
    const buttonText = getByText("Test Button");
    expect(buttonText).toBeInTheDocument();
  });

  it("calls onClick prop when clicked", () => {
    const handleClick = jest.fn();
    const { getByText } = render(
      <Button onClick={handleClick} text="Test Button" color="bg-red-500" />
    );
    fireEvent.click(getByText("Test Button"));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});

describe("OnlineUser", () => {
  it("displays the correct time in AM format", () => {
    const mockUser = {
      username: "TestUser",
      joinedAt: new Date("5/29/2024, 4:53:06 AM").toISOString(), // 9 AM UTC
    };
    const { getByText } = render(<OnlineUser {...mockUser} />);
    expect(getByText("4:53 AM")).toBeInTheDocument();
  });

  it("displays the correct time in PM format", () => {
    const mockUser = {
      username: "TestUser",
      joinedAt: new Date("5/29/2024, 4:53:06 PM").toISOString(), // 9 PM UTC
    };
    const { getByText } = render(<OnlineUser {...mockUser} />);
    expect(getByText("16:53 PM")).toBeInTheDocument();
  });
});

describe("TextInput", () => {
  it("renders correctly with the provided value and placeholder", () => {
    const { getByPlaceholderText } = render(
      <TextInput value="Test" onChange={jest.fn()} placeholder="Enter text" />
    );

    expect(getByPlaceholderText("Enter text")).toHaveValue("Test");
  });

  it("calls the onChange handler when the input value changes", () => {
    const onChange = jest.fn();
    const { getByPlaceholderText } = render(
      <TextInput value="" onChange={onChange} placeholder="Enter text" />
    );

    fireEvent.change(getByPlaceholderText("Enter text"), {
      target: { value: "New text" },
    });

    expect(onChange).toHaveBeenCalled();
  });
});

describe("ChatBalloon", () => {
  it("renders without crashing", () => {
    const message = {
      username: "user1",
      text: "Hello",
      timestamp: "2022-01-01T00:00:00Z",
    };

    render(<ChatBalloon message={message} usernameLocal="user1" />);
  });

  it("renders the correct username and text", () => {
    const message = {
      username: "user1",
      text: "Hello",
      timestamp: "2022-01-01T00:00:00Z",
    };

    const { getByText } = render(
      <ChatBalloon message={message} usernameLocal="user1" />
    );

    expect(getByText(message.username)).toBeInTheDocument();
    expect(getByText(message.text)).toBeInTheDocument();
  });
});
