import React from "react";
import { fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom";
import { GlobalContext } from "../app/_context/global-context";
import SignInPage from "@/app/(acount)/sign-in/page";

jest.mock("../app/_context/global-context", () => {
  const originalModule = jest.requireActual("../app/_context/global-context");
  return {
    __esModule: true,
    ...originalModule,
    GlobalContext: {
      ...originalModule.GlobalContext,
      Provider: ({ children, value }: any) => (
        <originalModule.GlobalContext.Provider value={value}>
          {children}
        </originalModule.GlobalContext.Provider>
      ),
    },
  };
});

jest.mock("../core/di/ioc", () => ({
  resolve: jest.fn().mockReturnValue({
    username: "",
    handleUsernameChange: jest.fn(),
    handleSubmit: jest.fn(),
    error: "",
    errorValidation: "",
    handleGoogleSignIn: jest.fn(),
  }),
}));

describe("SignInPage component", () => {
  test("renders without crashing", () => {
    render(<SignInPage />);
  });
  test("renders loading state correctly", () => {
    const { getByTestId } = render(
      <GlobalContext.Provider
        value={{
          isError: false,
          isLoading: true,
          updateIsError: jest.fn(),
          updateIsLoading: jest.fn(),
        }}
      >
        <SignInPage />
      </GlobalContext.Provider>
    );
    const LoadingSpinner = getByTestId("LoadingSpinner");

    expect(LoadingSpinner).toBeInTheDocument();
  });
  test("triggers sign in when Sign In button is clicked", async () => {
    const { getByTestId } = render(
      <GlobalContext.Provider
        value={{
          isError: false,
          isLoading: false,
          updateIsError: jest.fn(),
          updateIsLoading: jest.fn(),
        }}
      >
        <SignInPage />
      </GlobalContext.Provider>
    );

    const signInButton = getByTestId("signInButton");

    // Mock the sign in function
    const signInFunction = jest.fn();
    signInButton.onclick = signInFunction;

    // Click the sign in button
    fireEvent.click(signInButton);

    // Check that the sign in function was called
    expect(signInFunction).toHaveBeenCalled();
  });
  //   test("shows error validation when Sign In button is clicked without entering a username", async () => {
  //     render(<SignInPage />);

  //     const signInButton = screen.getByTestId("signInButton");
  //     fireEvent.click(signInButton);

  //     const errorValidationMessage = await screen.findByText(
  //       "Username is required"
  //     );
  //     expect(errorValidationMessage).toBeInTheDocument();
  //   });

  //   test("does not render loading state when isLoading is false", async () => {
  //     const { queryByTestId } = render(
  //       <GlobalContext.Provider
  //         value={{
  //           isError: false,
  //           isLoading: false,
  //           updateIsError: jest.fn(),
  //           updateIsLoading: jest.fn(),
  //         }}
  //       >
  //         <SignInPage />
  //       </GlobalContext.Provider>
  //     );

  //     await waitFor(() => {
  //       const LoadingSpinner = queryByTestId("LoadingSpinner");
  //       expect(LoadingSpinner).not.toBeInTheDocument();
  //     });
  //   });

  //   test("renders error message when isError is true", () => {
  //     const { getByText } = render(
  //       <GlobalContext.Provider
  //         value={{
  //           isError: true,
  //           isLoading: false,
  //           updateIsError: jest.fn(),
  //           updateIsLoading: jest.fn(),
  //         }}
  //       >
  //         <SignInPage />
  //       </GlobalContext.Provider>
  //     );

  //     const ErrorMessage = getByText(/error message/i);
  //     expect(ErrorMessage).toBeInTheDocument();
  //   });
});
