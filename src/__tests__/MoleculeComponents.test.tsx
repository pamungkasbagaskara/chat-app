import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { ChatArea, ChatInput, OnlineUsers } from "@/app/_components/molecules";

type Message = {
  username: string;
  text: string;
  timestamp: string;
};
describe("ChatArea", () => {
  window.HTMLElement.prototype.scrollIntoView = function () {};
  test("renders messages", () => {
    const mockMessages = [
      {
        username: "testUser",
        text: "testMessage",
        timestamp: "2022-01-01T00:00:00Z",
      },
    ];

    render(<ChatArea sortedMessages={mockMessages} usernameLocal="testUser" />);

    const messageElement = screen.getByText(/testMessage/i);
    expect(messageElement).toBeInTheDocument();
  });
});
interface ChatInputProps {
  newMessage: string;
  setNewMessage: (value: string) => void;
  sendMessage: () => void;
}
describe("ChatInput", () => {
  const setup = (props: ChatInputProps) => {
    const utils = render(<ChatInput {...props} />);
    const input = utils.getByPlaceholderText("Type a message...");
    const form = utils.getByTestId("form");
    return {
      input,
      form,
      ...utils,
    };
  };

  test("calls setNewMessage handler on input change", () => {
    const setNewMessage = jest.fn();
    const sendMessage = jest.fn();
    const { input } = setup({ newMessage: "", setNewMessage, sendMessage });

    fireEvent.change(input, { target: { value: "test message" } });
    expect(setNewMessage).toHaveBeenCalledWith("test message");
  });

  test("calls sendMessage handler on form submit", () => {
    const setNewMessage = jest.fn();
    const sendMessage = jest.fn();
    const { form } = setup({
      newMessage: "test message",
      setNewMessage,
      sendMessage,
    });

    fireEvent.submit(form);
    expect(sendMessage).toHaveBeenCalled();
  });
});
describe("OnlineUsers", () => {
  it("renders without crashing", () => {
    render(<OnlineUsers onlineUsers={[]} />);
  });

  it("renders the correct number of OnlineUser components", () => {
    const onlineUsers = [
      { username: "user1", joinedAt: "2022-01-01T00:00:00Z" },
      { username: "user2", joinedAt: "2022-01-01T00:01:00Z" },
    ];

    render(<OnlineUsers onlineUsers={onlineUsers} />);

    const userElements = screen.getAllByTestId("online-user");
    expect(userElements).toHaveLength(onlineUsers.length);
  });
});
