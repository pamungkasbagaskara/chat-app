/* eslint-disable no-unused-vars */
"use client";

import React, { createContext, useState } from "react";
export interface GlobalContextInterface {
  isError: boolean;
  isLoading: boolean;
  updateIsError: (val: boolean) => void;
  updateIsLoading: (val: boolean) => void;
}

const defaultState = {
  isError: false,
  isLoading: true,
};

export const GlobalContext =
  createContext<Partial<GlobalContextInterface>>(defaultState);

export const GlobalProvider = (props: any) => {
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const updateIsError = (val: boolean) => {
    setIsError(val);
  };
  const updateIsLoading = (val: boolean) => {
    setIsLoading(val);
  };

  return (
    <GlobalContext.Provider
      value={{
        isError: isError,
        isLoading: isLoading,
        updateIsError: updateIsError,
        updateIsLoading: updateIsLoading,
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
};
