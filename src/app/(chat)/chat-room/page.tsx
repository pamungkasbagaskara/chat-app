"use client";
import DI from "@/core/di/ioc";
import { OnlineUsers } from "@/app/_components/molecules";
import {
  ChatBox,
  ErrorModal,
  Loading,
  TextToggle,
} from "@/app/_components/organisms";
import { Button } from "@/app/_components/atoms";
import { useContext } from "react";
import { GlobalContext } from "@/app/_context/global-context";
import { faSignOut } from "@fortawesome/free-solid-svg-icons";

const ChatRoomPage: React.FC = () => {
  const {
    onlineUsers,
    handleSignOut,
    usernameLocal,
    messages,
    newMessage,
    setNewMessage,
    sendMessage,
    error,
    isOnlineUsersVisible,
    setIsOnlineUsersVisible,
  } = DI.resolve("ChatRoomViewmodel");
  const { isError, updateIsError, isLoading } = useContext(GlobalContext);
  const sortedMessages = messages.sort(
    (
      a: { timestamp: string | number | Date },
      b: { timestamp: string | number | Date }
    ) => new Date(a.timestamp).getTime() - new Date(b.timestamp).getTime()
  );

  return (
    <div className="h-[90vh] relative">
      {isLoading && <Loading />}
      <button
        className="md:hidden fixed top-0 left-1/2 transform -translate-x-1/2 m-2 px-2 py-1 z-50 "
        onClick={() => setIsOnlineUsersVisible(!isOnlineUsersVisible)}
      >
        <TextToggle
          text1={"Online Users"}
          text2={"Chat Room"}
          isText1Visible={isOnlineUsersVisible}
        />
      </button>
      <div className=" pt-[13%] md:pt-2 md:p-2 flex flex-wrap">
        <div
          className={`fixed top-0 left-0 w-full h-full pt-12 p-2 font-poppins mb-2 bg-white z-40 ${
            isOnlineUsersVisible ? "block" : "hidden"
          } md:hidden`}
        >
          <div className="w-full bg-secondary-light-blue p-2  rounded-lg h-[98vh] mr-2 max-h-full overflow-y-auto relative">
            <p className="text-xl text-bold text-secondary-blue">
              Online Users
            </p>
            <OnlineUsers onlineUsers={onlineUsers} />
            <div className="absolute bottom-0 right-0 m-2 px-2 py-1">
              <Button
                text="Sign Out"
                onClick={handleSignOut}
                color={"bg-primary-light-blue"}
                hoverColor="hover:bg-primary-red"
                isIconVisible={true}
                icon={faSignOut}
              />
            </div>
          </div>
        </div>
        <div className=" hidden md:block w-full md:w-1/4  font-poppins mb-2">
          <div className=" bg-secondary-light-blue p-2 rounded-lg h-[98vh] mr-2 max-h-full overflow-y-auto relative">
            <p className="text-xl text-bold text-secondary-blue">
              Online Users
            </p>
            <OnlineUsers onlineUsers={onlineUsers} />
            <div className="absolute bottom-0 right-0 m-2 px-2 py-1">
              <Button
                text="Sign Out"
                onClick={handleSignOut}
                color={"bg-primary-light-blue"}
                hoverColor="hover:bg-primary-red"
                isIconVisible={true}
                icon={faSignOut}
              />
            </div>
          </div>
        </div>
        <div className=" bg-secondary-light-blue p-2 w-full md:w-3/4 max-h-full h-[98vh] rounded-lg overflow-y-hidden">
          <ChatBox
            sortedMessages={sortedMessages}
            newMessage={newMessage}
            usernameLocal={usernameLocal}
            setNewMessage={setNewMessage}
            sendMessage={sendMessage}
          />
        </div>
      </div>
      <ErrorModal
        showModal={isError}
        title={"Error"}
        subtitle={error && error != "" ? error : "An error occurred"}
        onClickOK={() => updateIsError!(false)}
      />
    </div>
  );
};

export default ChatRoomPage;
