import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/navigation";
import { UserUsecaseInterface } from "@/core/user/domain/entities/user-usecase-interface";
import { ChatUsecaseInterface } from "@/core/chat/domain/entities/chat-usecase-interface";
import { Message } from "@/core/chat/domain/entities/message";
import { GlobalContext } from "@/app/_context/global-context";

type OnlineUser = {
  username: string;
  joinedAt: string;
};

interface ChatRoomViewmodelInterface {
  UserUsecase: UserUsecaseInterface;
  ChatUsecase: ChatUsecaseInterface;
}

export default function ChatRoomViewmodel({
  ChatUsecase,
  UserUsecase,
}: ChatRoomViewmodelInterface) {
  const router = useRouter();
  const [onlineUsers, setOnlineUsers] = useState<OnlineUser[]>([]);
  const [usernameLocal, setUsernameLocal] = useState<string | null>("");
  const [messages, setMessages] = useState<Message[]>([]);
  const [newMessage, setNewMessage] = useState("");
  const [error, setError] = useState("");
  const [isOnlineUsersVisible, setIsOnlineUsersVisible] = useState(false);

  const { updateIsError, updateIsLoading } = useContext(GlobalContext);
  //check if username is valid
  useEffect(() => {
    try {
      const usernameLocal = UserUsecase.getUsernameLocal();
      setUsernameLocal(usernameLocal);
      updateIsLoading!(true);
      if (usernameLocal) {
        UserUsecase.handleUserState(usernameLocal)
          .then((action: string) => {
            if (action === "redirectSignIn") {
              router.replace("/sign-in");
            }
            updateIsLoading!(false);
          })
          .catch((error: any) => {
            console.error("Error validating username", error);
            updateIsLoading!(false);
          });
      } else {
        updateIsLoading!(false);
        router.replace("/sign-in");
      }
    } catch (error) {
      console.error(`An error occurred in useEffect:`, error);
      updateIsError!(true);
      setError("An error occurred when validating username");
      // Handle the error here, e.g. show an error message to the user
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //subscribing to userservice, will update the online users state automatically
  useEffect(() => {
    try {
      UserUsecase.handleOnlineUsers(setOnlineUsers);
    } catch (error) {
      console.error("Error getting online users", error);
      updateIsError!(true);
      setError("An error occurred when getting online users");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSignOut = async () => {
    const usernameLocal = UserUsecase.getUsernameLocal();
    try {
      await UserUsecase.handleSignOut(usernameLocal);
      router.replace("/sign-in");
    } catch (error) {
      console.error("Error signing out: ", error);
      updateIsError!(true);
      setError("An error occurred when signing out");
    }
  };

  const sendMessage = async () => {
    try {
      const usernameLocal = UserUsecase.getUsernameLocal();
      if (newMessage && usernameLocal) {
        const message = {
          id: "",
          username: usernameLocal,
          text: newMessage,
          timestamp: Date.now(),
        };
        await ChatUsecase.addMessage(message);
        setNewMessage("");
      }
    } catch (error) {
      console.error(`An error occurred in sendMessage:`, error);
      updateIsError!(true);
      setError("An error occurred when sending a messasge");
      // Handle the error here, e.g. show an error message to the user
    }
  };

  const getLocalMessages = async () => {
    try {
      const usernameLocal = UserUsecase.getUsernameLocal();
      if (usernameLocal) {
        const localMessages = await ChatUsecase.getLocalMessages(usernameLocal);
        setMessages(localMessages);
      }
    } catch (error) {
      console.error(`An error occurred in getLocalMessages:`, error);
      updateIsError!(true);
      setError("An error occurred");
      // Handle the error here, e.g. show an error message to the user
    }
  };
  //getlocalmessage when first time loading the page
  useEffect(() => {
    const usernameLocal = UserUsecase.getUsernameLocal();
    if (usernameLocal) {
      getLocalMessages();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const isUniqueMessage = (prevMessages: Message[], newMsg: Message) => {
    return !prevMessages.some((prevMsg) => prevMsg.id === newMsg.id);
  };

  //append new message to message
  const appendMessages = (newMessages: Message[]) => {
    setMessages((prevMessages) => {
      const newUniqueMessages = newMessages.filter((newMsg) =>
        isUniqueMessage(prevMessages, newMsg)
      );
      return [...prevMessages, ...newUniqueMessages];
    });
  };

  //this will save the messages to indexedb everytime the messages state changes
  useEffect(() => {
    try {
      const usernameLocal = UserUsecase.getUsernameLocal();
      if (usernameLocal) {
        ChatUsecase.addMessageToLocal(messages, usernameLocal);
      }
    } catch (error) {
      console.error(`An error occurred in useEffect:`, error);
      updateIsError!(true);
      setError("An error occurred");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [messages]);

  //this will subscribe to chatservice to get the messages from the database,
  //with callback to add new message to the messages state
  useEffect(() => {
    try {
      const usernameLocal = UserUsecase.getUsernameLocal();
      if (usernameLocal) {
        ChatUsecase.getMessages(appendMessages);
      }
    } catch (error) {
      console.error(`An error occurred in useEffect:`, error);
      updateIsError!(true);
      setError("An error occurred");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    onlineUsers,
    handleSignOut,
    messages,
    newMessage,
    setMessages,
    setNewMessage,
    sendMessage,
    usernameLocal,
    error,
    isOnlineUsersVisible,
    setIsOnlineUsersVisible,
  };
}
