import React from "react";

interface OnlineUserProps {
  username: string;
  joinedAt: string;
}

const OnlineUser: React.FC<OnlineUserProps> = ({ username, joinedAt }) => {
  const date = new Date(joinedAt);
  const formattedTime = `${date.getHours()}:${date.getMinutes()} ${
    date.getHours() >= 12 ? "PM" : "AM"
  }`;

  return (
    <div
      data-testid="online-user"
      className=" flex justify-between items-center font-redditmono"
    >
      <p className="  font-roboto max-w-[75%] line-clamp-2 ">{username}</p>
      <p className=" text-xs text-[#1A0D0A]">{formattedTime}</p>
    </div>
  );
};

export default OnlineUser;
