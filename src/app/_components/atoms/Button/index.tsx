import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface ButtonProps {
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  text: string;
  color?: string;
  isIconVisible?: boolean;
  icon?: any;
  hoverColor?: string;
}

const Button: React.FC<ButtonProps> = ({
  onClick,
  text,
  color,
  isIconVisible,
  icon,
  hoverColor,
}) => {
  return (
    <button
      className={`rounded-xl p-2 px-4 w-full font-sans font-semibold text-white text-lg ${
        color ? color : "bg-primary-light-blue"
      }  ${hoverColor ? hoverColor : "hover:bg-primary-blue"} hover:text-white`}
      type="submit"
      onClick={onClick}
    >
      {isIconVisible && <FontAwesomeIcon icon={icon} />}
      {!isIconVisible && text}
    </button>
  );
};

export default Button;
