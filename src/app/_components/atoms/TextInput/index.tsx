import React from "react";

export interface TextInputProps {
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  placeholder: string;
}

const TextInput: React.FC<TextInputProps> = ({
  value,
  onChange,
  placeholder,
}) => {
  return (
    <input
      type="text"
      id="username"
      value={value}
      className="border-2 border-secondary-light-blue rounded-xl w-full p-2 mt-2 font-poppins text-sm"
      placeholder={placeholder}
      onChange={onChange}
    />
  );
};

export default TextInput;
