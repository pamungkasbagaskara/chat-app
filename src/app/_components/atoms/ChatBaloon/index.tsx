import React from "react";

interface Message {
  username: string;
  text: string;
  timestamp: string;
}

interface ChatBalloonProps {
  message: Message;
  usernameLocal: string;
  previousUsername?: string;
}

const ChatBalloon: React.FC<ChatBalloonProps> = ({
  message,
  usernameLocal,
  previousUsername,
}) => {
  return (
    <div data-testid="chat-balloon" className={` flex flex-col`}>
      {message.username !== previousUsername && (
        <p
          className={` font-extrabold font-roboto text-base mt-3  text-[#2E2825] ${
            message.username === usernameLocal ? "ml-auto " : "mr-auto "
          }`}
        >
          {message.username}
        </p>
      )}
      <div
        className={`max-w-[75%] w-fit rounded-2xl py-1 min-w-[20%] md:min-w-[8%] mt-1 font-poppins ${
          message.username === usernameLocal
            ? " ml-auto bg-white text-[#2E2825] pl-3 pr-2 mr-3 "
            : " mr-auto  bg-primary-blue text-white pl-2 pr-3 ml-3 "
        } ${message.username === previousUsername ? " mt-0" : ""} ${
          message.username === usernameLocal &&
          message.username !== previousUsername
            ? "rounded-tr-none"
            : ""
        } ${
          message.username !== usernameLocal &&
          message.username !== previousUsername
            ? "rounded-tl-none"
            : ""
        }`}
      >
        <p className=" text-sm ">{message.text}</p>
        <div className=" text-[0.65rem] flex justify-end  mt-1 font-redditmono font-extralight">
          <p className="">
            {new Date(message.timestamp).getHours()}:
            {new Date(message.timestamp)
              .getMinutes()
              .toString()
              .padStart(2, "0")}
          </p>
        </div>
      </div>
    </div>
  );
};

export default ChatBalloon;
