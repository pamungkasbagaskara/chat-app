import React, { useEffect } from "react";
import { ChatBalloon } from "../../atoms";

interface Message {
  username: string;
  text: string;
  timestamp: string;
}

interface ChatAreaProps {
  sortedMessages: Message[];
  usernameLocal: string;
}

const ChatArea: React.FC<ChatAreaProps> = ({
  sortedMessages,
  usernameLocal,
}) => {
  const messagesEndRef = React.useRef<HTMLDivElement>(null);
  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(scrollToBottom, [sortedMessages]);

  return (
    <div className="h-[90%] overflow-y-auto p-2 rounded-lg bg-secondary-light-blue">
      {sortedMessages ? (
        sortedMessages.map((message, index) => (
          <ChatBalloon
            key={index}
            message={message}
            usernameLocal={usernameLocal}
            previousUsername={
              index > 0 ? sortedMessages[index - 1].username : ""
            }
          />
        ))
      ) : (
        <div></div>
      )}

      <div ref={messagesEndRef} />
    </div>
  );
};

export default ChatArea;
