import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";

interface ChatInputProps {
  newMessage: string;
  setNewMessage: (value: string) => void;
  sendMessage: () => void;
}

const ChatInput: React.FC<ChatInputProps> = ({
  newMessage,
  setNewMessage,
  sendMessage,
}) => {
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    sendMessage();
  };

  return (
    <form
      data-testid="form"
      className="flex items-center justify-center mt-2 bg-white rounded-lg p-1 w-full focus-within:border-2 focus-within:border-primary-blue focus-within:outline-none"
      onSubmit={handleSubmit}
    >
      <input
        value={newMessage}
        placeholder="Type a message..."
        className="rounded-lg w-full p-2 mr-1 focus:outline-none focus:border-primary-blue"
        onChange={(e) => setNewMessage(e.target.value)}
      />
      <button
        type="submit"
        className={` p-1 px-3  rounded-lg  font-bold text-2xl ${
          newMessage
            ? " bg-secondary-blue text-white cursor-pointer"
            : " bg-primary-light-blue text-secondary-light-blue cursor-not-allowed"
        }`}
        disabled={!newMessage}
      >
        <FontAwesomeIcon icon={faPaperPlane} />
      </button>
    </form>
  );
};

export default ChatInput;
