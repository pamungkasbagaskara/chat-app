import React from "react";
import { OnlineUser } from "../../atoms";

interface User {
  username: string;
  joinedAt: string;
}

interface OnlineUsersProps {
  onlineUsers: User[];
}

const OnlineUsers: React.FC<OnlineUsersProps> = ({ onlineUsers }) => {
  return (
    <div className="">
      {onlineUsers.map((user) => (
        <div className=" p-1 mt-1 font-poppins" key={user.username}>
          <OnlineUser
            data-testid="online-user"
            username={user.username}
            joinedAt={user.joinedAt}
          />
        </div>
      ))}
    </div>
  );
};

export default OnlineUsers;
