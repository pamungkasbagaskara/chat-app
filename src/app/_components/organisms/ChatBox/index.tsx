import React from "react";
import { ChatArea, ChatInput } from "../../molecules";

interface Message {
  username: string;
  text: string;
  timestamp: string;
}

interface ChatBoxProps {
  sortedMessages: Message[];
  newMessage: string;
  usernameLocal: string;
  setNewMessage: (value: string) => void;
  sendMessage: () => void;
}

const ChatBox: React.FC<ChatBoxProps> = ({
  sortedMessages,
  newMessage,
  usernameLocal,
  setNewMessage,
  sendMessage,
}) => {
  return (
    <>
      <ChatArea sortedMessages={sortedMessages} usernameLocal={usernameLocal} />
      <ChatInput
        newMessage={newMessage}
        setNewMessage={setNewMessage}
        sendMessage={sendMessage}
      />
    </>
  );
};

export default ChatBox;
