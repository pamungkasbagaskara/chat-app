import React from "react";
import { Button } from "../../atoms";

interface ErrorModalProps {
  showModal: boolean | undefined;
  title?: string;
  subtitle?: string;
  onClickOK: () => void;
}

const ErrorModal = (props: ErrorModalProps) => {
  const { showModal, onClickOK, title, subtitle } = props;
  return (
    <>
      {showModal ? (
        <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-bgBlur">
          <div className="relative w-auto my-6 mx-auto max-w-3xl ">
            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
              <div className="flex items-start justify-center p-5 border-b border-solid border-secondary-gray rounded-t min-w-[90vw] lg:min-w-[30vw] ">
                <h3 className="text-3xl font=semibold">
                  {title && title !== "" ? title : "Error"}
                </h3>
              </div>
              <div className="relative p-6 flex-auto">
                {subtitle && subtitle !== ""
                  ? subtitle
                  : "There was an error processing your request"}
              </div>
              <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                <Button
                  text="Ok"
                  onClick={onClickOK}
                  color={"bg-primary-red"}
                />
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default ErrorModal;
