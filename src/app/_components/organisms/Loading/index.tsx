import React from "react";

export default function Loading() {
  return (
    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div
        data-testid="LoadingSpinner"
        className="animate-spin rounded-full h-32 w-32 border-b-4 border-primary-blue"
      ></div>
    </div>
  );
}
