import React from "react";

interface TextToggleProps {
  text1: string;
  text2: string;
  isText1Visible: boolean;
}

const TextToggle: React.FC<TextToggleProps> = ({
  text1,
  text2,
  isText1Visible,
}) => {
  return (
    <div className="flex items-center justify-center cursor-pointer">
      <span
        className={` p-1 rounded-l-lg min-w-[40vw] flex justify-center items-center ${
          isText1Visible
            ? " text-white bg-primary-blue"
            : " bg-secondary-gray text-white"
        }`}
      >
        {text1}
      </span>

      <span
        className={` p-1 rounded-r-lg min-w-[40vw] flex justify-center items-center ${
          isText1Visible
            ? " bg-secondary-gray text-white"
            : "text-white bg-primary-blue"
        }`}
      >
        {text2}
      </span>
    </div>
  );
};

export default TextToggle;
