"use client";
import React, { useContext } from "react";
import DI from "@/core/di/ioc";
import Image from "next/image";
import { Button, TextInput } from "@/app/_components/atoms";
import { ErrorModal, Loading } from "@/app/_components/organisms";
import { GlobalContext } from "@/app/_context/global-context";

const SignInPage: React.FC = () => {
  const {
    username,
    handleUsernameChange,
    handleSubmit,
    error,
    errorValidation,
    handleGoogleSignIn,
  } = DI.resolve("SignInViewmodel");
  const signInImage = "/signInImage.svg";
  const { isError, updateIsError, isLoading } = useContext(GlobalContext);
  return (
    <div className=" flex">
      {isLoading && <Loading />}
      <div className=" hidden lg:block w-1/2">
        <Image
          src={signInImage} // Route of the image file
          className=" h-screen w-full object-cover"
          height={500} // Desired size with correct aspect ratio
          width={500} // Desired size with correct aspect ratio
          alt="Description for the image"
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
        className="  w-full lg:w-1/2 font-poppins"
      >
        <div className=" w-[80%] md:w-[60%]  flex flex-col items-center justify-center space-y-4">
          <div className="  flex flex-col items-center mb-5">
            <p className=" text-xl tracking-widest">Welcome To</p>
            <p className=" mb-1 mt-2 text-4xl font-bold tracking-wider text-secondary-blue">
              Chat App
            </p>
            <p className=" text-sm tracking-wide text-primary-gray">
              Sign in to start Messaging
            </p>
          </div>
          <form
            data-testid="signInForm"
            onSubmit={handleSubmit}
            className=" w-full"
          >
            <div className="">
              <label htmlFor="username" className="  text-base font-poppins">
                Input Your Username:
              </label>
              <TextInput
                value={username}
                onChange={handleUsernameChange}
                placeholder="Type your username here"
              />
            </div>
            {errorValidation && (
              <div className=" text-primary-red font-poppins ">
                {errorValidation}
              </div>
            )}
            <div data-testid="signInButton" className="mt-3">
              <Button
                text="Sign In"
                onClick={handleSubmit}
                color={" bg-primary-light-blue"}
              />
            </div>
          </form>
          <div className=" w-full flex items-center justify-center my-5">
            <hr className="flex-grow border-secondary-gray" />
            <span className="px-2 text-primary-gray">Or</span>
            <hr className="flex-grow border-secondary-gray" />
          </div>{" "}
          <p className=" text-primary-gray mb-1 text-sm font-poppins">
            Continue with social media
          </p>
          <Button
            text="Google"
            onClick={(e) => {
              handleGoogleSignIn(e);
            }}
            color={"bg-primary-red"}
          />
        </div>
      </div>
      <ErrorModal
        showModal={isError}
        title={"Error"}
        subtitle={error && error != "" ? error : "An error occurred"}
        onClickOK={() => updateIsError!(false)}
      />
    </div>
  );
};

export default SignInPage;
