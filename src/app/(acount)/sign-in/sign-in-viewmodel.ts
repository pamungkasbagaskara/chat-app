"use client";
import { useState, useEffect, FormEvent, useContext } from "react";
import { useRouter } from "next/navigation";
import { UserUsecaseInterface } from "@/core/user/domain/entities/user-usecase-interface";
import { GlobalContext } from "@/app/_context/global-context";
interface SignInViewmodelInterface {
  UserUsecase: UserUsecaseInterface;
}
export default function SignInViewmodel({
  UserUsecase,
}: SignInViewmodelInterface) {
  const [username, setUsername] = useState("");
  const [errorValidation, setErrorValidation] = useState("");
  const [error, setError] = useState("");

  const { updateIsError, updateIsLoading } = useContext(GlobalContext);

  const router = useRouter();
  useEffect(() => {
    const usernameLocal = UserUsecase.getUsernameLocal();
    if (usernameLocal) {
      // updateIsLoading!(true);
      UserUsecase.handleUserState(usernameLocal)
        .then((action: string) => {
          if (action === "redirectChatRoom") {
            router.replace("/chat-room");
          } else if (action === "redirectSignIn") {
            setError("Username is taken, please try another one.");
          }
          updateIsLoading!(false);
        })
        .catch((error: any) => {
          updateIsError!(true);
          setError("An error occurred");
          console.error("Error validating username", error);
          updateIsLoading!(false);
        });
    } else {
      //no username
      updateIsLoading!(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGoogleSignIn = async (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
    const { isSuccess, username, errorValidation, error } =
      await UserUsecase.handleGoogleSignIn();
    if (isSuccess) {
      router.replace("/chat-room");
    } else if (errorValidation) {
      setUsername(username);
      setErrorValidation(errorValidation);
    } else if (error) {
      updateIsError!(true);
      setError(error);
    }
  };

  const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };
  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();
    updateIsLoading!(true);
    if (!username || username === "") {
      updateIsLoading!(false);
      return setErrorValidation("Username is required");
    } else {
      try {
        const result = await UserUsecase.handleSubmit(username);
        if (result.errorValidation) {
          setErrorValidation(result.errorValidation);
          updateIsLoading!(false);
        } else {
          updateIsLoading!(false);
          router.replace("/chat-room");
        }
      } catch (error) {
        updateIsLoading!(false);
        console.error(`An error occurred in handleSubmit:`, error);
        updateIsError!(true);
        setError("An error occurred while processing your request.");
      }
    }
  };

  return {
    username,
    setUsername,
    handleUsernameChange,
    handleSubmit,
    error,
    errorValidation,
    handleGoogleSignIn,
  };
}
