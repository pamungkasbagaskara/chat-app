"use client";

import * as Sentry from "@sentry/nextjs";
import Error from "next/error";
import { useEffect } from "react";

import PropTypes from "prop-types";

export default function GlobalError({ error }) {
  useEffect(() => {
    Sentry.captureException(error);
  }, [error]);

  return (
    <html lang="en">
      <body>
        <Error />
      </body>
    </html>
  );
}

GlobalError.propTypes = {
  error: PropTypes.object.isRequired,
};
