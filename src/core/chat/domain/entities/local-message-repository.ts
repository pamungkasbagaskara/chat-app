import { Message } from "./message";

export interface LocalMessageRepository {
  addMessageToLocal: (
    newMessages: Message[],
    usernameLocal: string
  ) => Promise<void>;
  getLocalMessages: (username: string) => Promise<Message[]>;
}
