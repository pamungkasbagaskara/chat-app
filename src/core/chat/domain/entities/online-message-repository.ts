import { Message } from "./message";

export interface OnlineMessageRepository {
  getMessages: (callback: (messages: Message[]) => void) => void;
  addMessage: (message: Message) => Promise<void>;
}
