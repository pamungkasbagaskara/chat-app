import { Message } from "./message";

export interface ChatUsecaseInterface {
  addMessage(message: Message): Promise<void>;
  getMessages(callback: (messages: Message[]) => void): void;
  addMessageToLocal(
    newMessages: Message[],
    usernameLocal: string | null
  ): Promise<void>;
  getLocalMessages(username: string | null): Promise<Message[]>;
}
