import { LocalMessageRepository } from "./local-message-repository";
import { OnlineMessageRepository } from "./online-message-repository";

export interface IMessageRepository {
  OnlineMessageRepositoryImpl: OnlineMessageRepository;
  LocalMessageRepositoryImpl: LocalMessageRepository;
}
