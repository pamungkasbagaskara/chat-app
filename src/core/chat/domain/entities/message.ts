export interface Message {
  id: string | null;
  username: string;
  text: string;
  timestamp: number | string;
}
