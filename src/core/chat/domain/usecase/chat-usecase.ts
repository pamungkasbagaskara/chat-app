import { IMessageRepository } from "../entities/Imessage-repository";
import { Message } from "../entities/message";
import { ChatUsecaseInterface } from "../entities/chat-usecase-interface";

export function chatUsecase({
  OnlineMessageRepositoryImpl,
  LocalMessageRepositoryImpl,
}: IMessageRepository): ChatUsecaseInterface {
  return {
    async addMessage(message: Message) {
      try {
        await OnlineMessageRepositoryImpl.addMessage(message);
      } catch (error) {
        console.error(`An error occurred in addMessage:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    getMessages(callback: (messages: Message[]) => void) {
      try {
        OnlineMessageRepositoryImpl.getMessages(callback);
      } catch (error) {
        console.error(`An error occurred in getMessages:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    async addMessageToLocal(newMessages: Message[], usernameLocal: string) {
      try {
        await LocalMessageRepositoryImpl.addMessageToLocal(
          newMessages,
          usernameLocal
        );
      } catch (error) {
        console.error(`An error occurred in addMessageToLocal:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    async getLocalMessages(username: string) {
      try {
        return await LocalMessageRepositoryImpl.getLocalMessages(username);
      } catch (error) {
        console.error(`An error occurred in getLocalMessages:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
  };
}
