import { ILocalMessageStorage } from "@/core/infrastructures/protocols/storage/ILocalMessageStorage";
import { LocalMessageRepository } from "../../domain/entities/local-message-repository";
import { Message } from "../../domain/entities/message";

export function LocalMessageRepositoryImpl({
  LocalMessageAdapter,
}: {
  LocalMessageAdapter: ILocalMessageStorage;
}): LocalMessageRepository {
  return {
    getLocalMessages(username: string): Promise<Message[]> {
      return LocalMessageAdapter.getMessages(username).catch((error: any) => {
        console.error("Error in getMessagesFromIndexDB:", error);
        return []; // return an empty array in case of error
      });
    },

    async addMessageToLocal(newMessages: Message[], usernameLocal: string) {
      const promises = newMessages.map((message: Message) =>
        LocalMessageAdapter.addMessage(message, usernameLocal).catch(
          (error: any) => {
            console.error("Error adding message to IndexedDB:", error);
          }
        )
      );

      // Wait for all promises to resolve before returning
      await Promise.all(promises);
    },
  };
}
