import { OnlineMessageRepository } from "../../domain/entities/online-message-repository";
import { Message } from "../../domain/entities/message";
import { IOnlineMessageStorage } from "@/core/infrastructures/protocols/storage/IOnlineMessageStorage";

export function OnlineMessageRepositoryImpl({
  OnlineMessageAdapter,
}: {
  OnlineMessageAdapter: IOnlineMessageStorage;
}): OnlineMessageRepository {
  return {
    getMessages(callback: (messages: Message[]) => void): void {
      try {
        OnlineMessageAdapter.getMessages((messages) => {
          callback(messages);
        });
      } catch (error) {
        console.error(`An error occurred in getMessages:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },

    async addMessage(message: Message): Promise<void> {
      try {
        return await OnlineMessageAdapter.addMessage(message);
      } catch (error) {
        console.error(`An error occurred in addMessage:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
  };
}
