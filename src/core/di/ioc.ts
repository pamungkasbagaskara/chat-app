import { asFunction, createContainer } from "awilix";

import { userUsecase } from "../user/domain/usecase/user-usecase";
import SignInViewmodel from "@/app/(acount)/sign-in/sign-in-viewmodel";
import { onlineUserRepositoryImpl } from "../user/data/implementations/online-user-repository-impl";
import { localUserRepositoryImpl } from "../user/data/implementations/local-user-repository-impl";
import { localUserAdapter } from "../infrastructures/adapter/storage/local-user-adapter";
import { onlineUserAdapter } from "../infrastructures/adapter/storage/online-user-adapter";
import { localMessageAdapter } from "../infrastructures/adapter/storage/local-message-adapter";
import { chatUsecase } from "../chat/domain/usecase/chat-usecase";
import ChatRoomViewmodel from "@/app/(chat)/chat-room/chat-room-viewmodel";
import { OnlineMessageRepositoryImpl } from "../chat/data/implementations/online-message-repository-impl";
import { LocalMessageRepositoryImpl } from "../chat/data/implementations/local-message-repository-impl";
import { OnlineMessageAdapter } from "../infrastructures/adapter/storage/online-message-adapter";

// Create the container
const container = createContainer();

// Register your dependencies
container.register({
  //viewmodel dependencies
  SignInViewmodel: asFunction(SignInViewmodel),
  ChatRoomViewmodel: asFunction(ChatRoomViewmodel),
  //usecase dependencies
  ChatUsecase: asFunction(chatUsecase),
  UserUsecase: asFunction(userUsecase),
  //repository dependencies
  OnlineUserRepositoryImpl: asFunction(onlineUserRepositoryImpl),
  LocalUserRepositoryImpl: asFunction(localUserRepositoryImpl),
  OnlineMessageRepositoryImpl: asFunction(OnlineMessageRepositoryImpl),
  LocalMessageRepositoryImpl: asFunction(LocalMessageRepositoryImpl),
  //adapter dependencies
  LocalUserAdapter: asFunction(localUserAdapter),
  OnlineStorageAdapter: asFunction(onlineUserAdapter),
  LocalMessageAdapter: asFunction(localMessageAdapter),
  OnlineMessageAdapter: asFunction(OnlineMessageAdapter),
});
export default container;
