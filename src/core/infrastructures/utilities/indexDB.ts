import Dexie from "dexie";

export const db = new Dexie("ChatDatabase");
db.version(1).stores({
  messages: "++id, timestamp, saverUsername",
});
