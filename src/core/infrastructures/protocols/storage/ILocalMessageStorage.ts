import { Message } from "@/core/chat/domain/entities/message";

export interface ILocalMessageStorage {
  getMessages(username: string): Promise<Message[]>;
  addMessage(message: Message, usernameLocal: string): Promise<void>;
}
