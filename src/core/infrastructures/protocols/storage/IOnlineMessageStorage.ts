import { Message } from "@/core/chat/domain/entities/message";

export interface IOnlineMessageStorage {
  getMessages(callback: (messages: Message[]) => void): void;
  addMessage: (message: Message) => Promise<void>;
}
