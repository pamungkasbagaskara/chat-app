// online-storage-adapter.ts
import {
  GoogleAuthProvider,
  getAuth,
  signInAnonymously,
  signInWithPopup,
  signOut,
} from "firebase/auth";
import {
  equalTo,
  get,
  onDisconnect,
  onValue,
  orderByChild,
  query,
  ref,
  set,
  update,
} from "firebase/database";
import { database } from "@/core/infrastructures/utilities/firebase";
import { IOnlineStorage } from "../../protocols/storage/IOnlineStorage";

export function onlineUserAdapter(): IOnlineStorage {
  const auth = getAuth();

  return {
    signOutFirebaseRTDB: async (username: string) => {
      try {
        const userRef = ref(database, `users/${username}`);
        await update(userRef, {
          state: "offline",
          last_changed: Date.now(),
          user_id: auth?.currentUser?.uid,
        });
      } catch (error) {
        console.error(`An error occurred in signOutFirebaseRTDB:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    signOutFirebaseAuth: async () => {
      try {
        await signOut(auth);
      } catch (error) {
        console.error(`An error occurred in signOutFirebaseAuth:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    getUserData: async (username: string) => {
      try {
        const userRef = ref(database, `users/${username}`);
        const snapshot = await get(userRef);
        if (snapshot.exists()) {
          return snapshot.val();
        }
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    updateUserState: async (username: string, state: string, time: number) => {
      try {
        const userRef = ref(database, `users/${username}`);
        await update(userRef, {
          state: state,
          last_changed: time,
        });
      } catch (error) {
        console.error(`An error occurred in updateUserState:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    setOnDisconnect: (usernameLocal: string, state: string) => {
      try {
        const onDisconnectRef = ref(database, `users/${usernameLocal}/state`);
        onDisconnect(onDisconnectRef).set(state);
      } catch (error) {
        console.error(`An error occurred in setOnDisconnect:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    observeUsers: (callback: (users: any) => void) => {
      try {
        const usersRef = ref(database, "users");
        const onlineUsersQuery = query(
          usersRef,
          orderByChild("state"),
          equalTo("online")
        );
        onValue(onlineUsersQuery, (snapshot) => {
          callback(snapshot.val());
        });
      } catch (error) {
        console.error(`An error occurred in observeUsers:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    signInWithGoogle: async () => {
      try {
        const provider = new GoogleAuthProvider();
        return await signInWithPopup(auth, provider);
      } catch (error) {
        console.error(`An error occurred in signInWithGoogle:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    signInAnonymously: async () => {
      try {
        return await signInAnonymously(auth);
      } catch (error) {
        console.error(`An error occurred in signInAnonymously:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    checkIfUserExists: async (username: string) => {
      try {
        const userRef = ref(database, `users/${username}`);
        return await get(userRef);
      } catch (error) {
        console.error(`An error occurred in checkIfUserExists:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    setUser: async (username: string, uid: string) => {
      try {
        const userRef = ref(database, `users/${username}`);
        return await set(userRef, {
          state: "offline",
          last_changed: Date.now(),
          user_id: uid,
        });
      } catch (error) {
        console.error(`An error occurred in setUser:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    getCurrentUserId: () => {
      return auth.currentUser?.uid ?? "";
    },
  };
}
