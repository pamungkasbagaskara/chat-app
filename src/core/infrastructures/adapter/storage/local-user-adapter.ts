import { ILocalStorage } from "../../protocols/storage/ILocalStorage";

export function localUserAdapter(): ILocalStorage {
  return {
    getItem(key: string): string | null {
      try {
        return localStorage.getItem(key);
      } catch (error) {
        console.error(`An error occurred in getItem:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    setItem(key: string, value: string): void {
      try {
        localStorage.setItem(key, value);
      } catch (error) {
        console.error(`An error occurred in setItem:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    removeItem(key: string): void {
      try {
        localStorage.removeItem(key);
      } catch (error) {
        console.error(`An error occurred in removeItem:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
  };
}
