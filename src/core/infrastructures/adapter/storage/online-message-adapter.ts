import {
  ref,
  push,
  set,
  onChildAdded,
  orderByChild,
  startAt,
  query,
} from "firebase/database";
import { database } from "../../utilities/firebase";
import { IOnlineMessageStorage } from "../../protocols/storage/IOnlineMessageStorage";
import { Message } from "@/core/chat/domain/entities/message";

export function OnlineMessageAdapter(): IOnlineMessageStorage {
  return {
    getMessages(callback: (messages: Message[]) => void): void {
      try {
        const messagesRef = ref(database, "messages");
        const twoMinutesAgo = Date.now() - 2 * 60 * 1000; // 2 minutes ago in milliseconds
        const firebaseQuery = query(
          messagesRef,
          orderByChild("timestamp"),
          startAt(twoMinutesAgo)
        );
        const messages: Message[] = [];
        onChildAdded(firebaseQuery, (snapshot) => {
          const data = snapshot.val();
          const newMessage = {
            id: snapshot.key,
            username: data.username,
            text: data.text,
            timestamp: new Date(data.timestamp).toLocaleString(),
          };
          messages.push(newMessage);

          callback(messages);
        });
      } catch (error) {
        console.error(`An error occurred in getMessages:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },

    addMessage: (message: Message): Promise<void> => {
      try {
        const messagesRef = ref(database, "messages");
        const newMessageRef = push(messagesRef);
        return set(newMessageRef, {
          username: message.username,
          text: message.text,
          timestamp: message.timestamp,
        });
      } catch (error) {
        console.error(`An error occurred in addMessage:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
  };
}
