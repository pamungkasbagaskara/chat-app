// localMessageAdapter.ts

import { Message } from "@/core/chat/domain/entities/message";
import { db } from "../../../infrastructures/utilities/indexDB"; // import your db instance
import { ILocalMessageStorage } from "../../protocols/storage/ILocalMessageStorage";

export function localMessageAdapter(): ILocalMessageStorage {
  return {
    getMessages(username: string): Promise<Message[]> {
      return db
        .table("messages")
        .where("saverUsername")
        .equals(username)
        .toArray()
        .catch((error) => {
          console.error(`An error occurred in getMessages:`, error);
          throw error; // re-throw the error so it can be caught and handled by the calling code
        });
    },

    addMessage(message: Message, usernameLocal: string): Promise<void> {
      return db
        .table("messages")
        .where("timestamp")
        .equals(message.timestamp)
        .and((message: any) => message.saverUsername === usernameLocal)
        .count()
        .then((count: any) => {
          if (count === 0) {
            // If the message is not already in IndexedDB, add it
            return db
              .table("messages")
              .put({
                ...message,
                saverUsername: usernameLocal,
              })
              .then(() => {})
              .catch((error) => {
                console.error(`An error occurred in addMessage:`, error);
                throw error; // re-throw the error so it can be caught and handled by the calling code
              });
          }
        })
        .catch((error) => {
          console.error(`An error occurred in addMessage:`, error);
          throw error; // re-throw the error so it can be caught and handled by the calling code
        });
    },
  };
}
