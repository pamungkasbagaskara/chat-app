import { UserRepository } from "../../data/implementations/user-repository-impl";
import { UserUsecaseInterface } from "../entities/user-usecase-interface";

export function userUsecase({
  OnlineUserRepositoryImpl,
  LocalUserRepositoryImpl,
}: UserRepository): UserUsecaseInterface {
  return {
    async handleSignOut(username: string): Promise<void> {
      try {
        await OnlineUserRepositoryImpl.signOutFirebaseRTDB(username);
        await OnlineUserRepositoryImpl.signOutFirebaseAuth();
        LocalUserRepositoryImpl.removeUsernameLocal();
      } catch (error) {
        console.error("Error signing out: ", error);
        throw error;
      }
    },
    async handleUserState(usernameLocal: string): Promise<string> {
      try {
        if (usernameLocal) {
          const userData = await OnlineUserRepositoryImpl.getUserData(
            usernameLocal
          );
          if (userData) {
            if (
              userData.state === "online" &&
              userData.user_id !== OnlineUserRepositoryImpl.getCurrentUserId()
            ) {
              return "redirectSignIn";
            } else if (userData.state === "offline") {
              await OnlineUserRepositoryImpl.updateUserState(
                usernameLocal,
                "online",
                Date.now()
              );
              OnlineUserRepositoryImpl.setOnDisconnect(
                usernameLocal,
                "offline"
              );
              return "redirectChatRoom";
            }
          } else {
            LocalUserRepositoryImpl.removeUsernameLocal();
            return "redirectSignIn";
          }
        } else {
          LocalUserRepositoryImpl.removeUsernameLocal();
          return "redirectSignIn";
        }

        // Default return statement
        return "defaultAction";
      } catch (error) {
        console.error(`An error occurred in handleUserState:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    handleOnlineUsers(setOnlineUsers: any): void {
      try {
        OnlineUserRepositoryImpl.observeUsers((users: any) => {
          if (users) {
            const onlineUsers = Object.keys(users)
              .filter((key) => users[key].state === "online")
              .map((key) => ({
                username: key,
                joinedAt: new Date(users[key].last_changed).toLocaleString(),
              }));
            setOnlineUsers(onlineUsers);
          }
        });
      } catch (error) {
        console.error(`An error occurred in handleOnlineUsers:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    async handleGoogleSignIn(): Promise<{
      isSuccess: boolean;
      username: string;
      errorValidation?: string;
      error?: string;
    }> {
      try {
        const result = await OnlineUserRepositoryImpl.signInWithGoogle();
        const user = result.user;

        const snapshot = await OnlineUserRepositoryImpl.checkIfUserExists(
          user.displayName
        );
        if (snapshot.exists()) {
          const userData = snapshot.val();
          if (userData.state === "online") {
            return {
              isSuccess: false,
              username: user.displayName,
              errorValidation: "Username is taken",
            };
          }
        }
        await OnlineUserRepositoryImpl.setUser(user.displayName, user.uid);
        OnlineUserRepositoryImpl.setOnDisconnect(user.displayName, "offline");
        LocalUserRepositoryImpl.setUsername(user.displayName ?? "");
        return { isSuccess: true, username: user.displayName };
      } catch (error: any) {
        console.error(`An error occurred in handleSubmit:`, error);
        return { isSuccess: false, username: "", error: error.message };
      }
    },
    async handleSubmit(username: string) {
      try {
        const userCredential =
          await OnlineUserRepositoryImpl.signInAnonymously();
        const userData = await OnlineUserRepositoryImpl.getUserData(username);
        if (userData && userData.state === "online") {
          return {
            errorValidation: "Username is taken, please try another one.",
          };
        }
        await OnlineUserRepositoryImpl.setUser(
          username,
          userCredential.user.uid
        );
        OnlineUserRepositoryImpl.setOnDisconnect(username, "offline");
        LocalUserRepositoryImpl.setUsername(username);
        return { success: true };
      } catch (error) {
        console.error(`An error occurred in handleSubmit:`, error);
        return { error: "An error occurred while processing your request." };
      }
    },
    getUsernameLocal(): string | null {
      return LocalUserRepositoryImpl.getUsernameLocal();
    },
  };
}
