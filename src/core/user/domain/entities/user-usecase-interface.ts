export interface UserUsecaseInterface {
  handleSignOut(username: string | null): Promise<void>;
  handleUserState(usernameLocal: string | null): Promise<string>;
  handleOnlineUsers(setOnlineUsers: any): void;
  handleGoogleSignIn(): Promise<{
    isSuccess: boolean;
    username: string;
    errorValidation?: string;
    error?: string;
  }>;
  handleSubmit(
    username: string | null
  ): Promise<{ error?: string; success?: boolean; errorValidation?: string }>;
  getUsernameLocal(): string | null;
}
