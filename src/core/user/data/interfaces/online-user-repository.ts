export interface OnlineUserRepository {
  signOutFirebaseRTDB: (username: string) => Promise<void>;
  signOutFirebaseAuth: () => Promise<void>;
  getUserData: (username: string) => Promise<any>;
  updateUserState: (
    username: string,
    state: string,
    time: number
  ) => Promise<void>;
  setOnDisconnect: (usernameLocal: string, state: string) => void;
  observeUsers: (callback: (users: any) => void) => void;
  signInWithGoogle: () => Promise<any>;
  signInAnonymously: () => Promise<any>;
  checkIfUserExists: (username: string) => Promise<any>;
  setUser: (username: string, uid: string) => Promise<void>;
  getCurrentUserId: () => string | null;
}
