export interface LocalUserRepository {
  removeUsernameLocal(): void; // Add return type annotation
  setUsername(username: string): void; // Add return type annotation
  getUsernameLocal(): string | null;
}
