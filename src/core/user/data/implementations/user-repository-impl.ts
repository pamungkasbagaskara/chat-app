import { LocalUserRepository } from "../interfaces/local-user-repository";
import { OnlineUserRepository } from "../interfaces/online-user-repository";

export interface UserRepository {
  OnlineUserRepositoryImpl: OnlineUserRepository;
  LocalUserRepositoryImpl: LocalUserRepository;
}
