import { ILocalStorage } from "@/core/infrastructures/protocols/storage/ILocalStorage";
import { LocalUserRepository } from "../interfaces/local-user-repository";

export function localUserRepositoryImpl({
  LocalUserAdapter,
}: {
  LocalUserAdapter: ILocalStorage;
}): LocalUserRepository {
  return {
    removeUsernameLocal(): void {
      LocalUserAdapter.removeItem("username");
    },
    setUsername(username: string): void {
      try {
        LocalUserAdapter.setItem("username", username);
      } catch (error) {
        console.error(`An error occurred in setUsername:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
    getUsernameLocal(): string | null {
      try {
        return LocalUserAdapter.getItem("username");
      } catch (error) {
        console.error(`An error occurred in getUsernameLocal:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },
  };
}
