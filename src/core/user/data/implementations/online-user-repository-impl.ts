import { IOnlineStorage } from "@/core/infrastructures/protocols/storage/IOnlineStorage";
import { OnlineUserRepository } from "../interfaces/online-user-repository";

export function onlineUserRepositoryImpl({
  OnlineStorageAdapter,
}: {
  OnlineStorageAdapter: IOnlineStorage;
}): OnlineUserRepository {
  return {
    signOutFirebaseRTDB: (username: string) => {
      return new Promise<void>((resolve, reject) => {
        try {
          OnlineStorageAdapter.signOutFirebaseRTDB(username);
          resolve();
        } catch (error: any) {
          console.error(`An error occurred in signOutFirebaseRTDB:`, error);
          reject(new Error(error)); // reject the promise with the error so it can be caught and handled by the calling code
        }
      });
    },
    signOutFirebaseAuth: () => {
      return new Promise<void>((resolve, reject) => {
        try {
          OnlineStorageAdapter.signOutFirebaseAuth();
          resolve();
        } catch (error: any) {
          console.error(`An error occurred in signOutFirebaseAuth:`, error);
          reject(new Error(error)); // reject the promise with the error so it can be caught and handled by the calling code
        }
      });
    },
    getUserData: (username: string): Promise<any> => {
      return new Promise<any>((resolve, reject) => {
        OnlineStorageAdapter.getUserData(username)
          .then((userData: any) => {
            resolve(userData);
          })
          .catch((error: any) => {
            reject(new Error(error));
          });
      });
    },
    updateUserState: (
      username: string,
      state: string,
      time: number
    ): Promise<void> => {
      return new Promise<void>((resolve, reject) => {
        OnlineStorageAdapter.updateUserState(username, state, time)
          .then(() => {
            resolve();
          })
          .catch((error: any) => {
            reject(new Error(error));
          });
      });
    },

    setOnDisconnect: (username: string, state: string): void => {
      try {
        OnlineStorageAdapter.setOnDisconnect(username, state);
      } catch (error) {
        console.error(`An error occurred in setOnDisconnect:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },

    observeUsers: (callback: (users: any) => void): void => {
      try {
        OnlineStorageAdapter.observeUsers(callback);
      } catch (error) {
        console.error(`An error occurred in observeUsers:`, error);
        throw error; // re-throw the error so it can be caught and handled by the calling code
      }
    },

    signInWithGoogle: (): Promise<any> => {
      return new Promise<any>((resolve, reject) => {
        OnlineStorageAdapter.signInWithGoogle()
          .then((result: any) => {
            resolve(result);
          })
          .catch((error: any) => {
            reject(new Error(error));
          });
      });
    },

    signInAnonymously: (): Promise<any> => {
      return new Promise<any>((resolve, reject) => {
        OnlineStorageAdapter.signInAnonymously()
          .then((result: any) => {
            resolve(result);
          })
          .catch((error: any) => {
            reject(new Error(error));
          });
      });
    },

    checkIfUserExists: (username: string): Promise<any> => {
      return new Promise<any>((resolve, reject) => {
        OnlineStorageAdapter.checkIfUserExists(username)
          .then((exists: any) => {
            resolve(exists);
          })
          .catch((error: any) => {
            reject(new Error(error));
          });
      });
    },

    setUser: (username: string, uid: string): Promise<void> => {
      return new Promise<void>((resolve, reject) => {
        OnlineStorageAdapter.setUser(username, uid)
          .then(() => {
            resolve();
          })
          .catch((error: any) => {
            reject(new Error(error));
          });
      });
    },

    getCurrentUserId: (): string => {
      return OnlineStorageAdapter.getCurrentUserId();
    },
  };
}
