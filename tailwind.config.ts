import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      "2xl": "1536px",
    },
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
        roboto: ["Roboto", "sans-serif"],
        redditmono: ["Redditmono", "monospace"],
      },
      colors: {
        "primary-red": "#EA4335",
        "primary-purple": "#ad88c6",
        "dark-purple": "#554994",
        "primary-gray": "#6b7280",
        "primary-orange": "#DEB6AB",
        "primary-green": "#22c55e",
        "secondary-gray": "#d1d5db",
        "secondary-purple": "#E1AFD1",
        "secondary-white": "#FFF9F9",
        "secondary-green": "#bbf7d0",
        "secondary-red": "#fecaca",
        "tertiary-purple": "#7469B6",
        "tertiary-red": "#b25454",
        "primary-blue": "#205CF5",
        "secondary-blue": "#002BFE",
        "primary-light-blue": "#6D97DC",
        "secondary-light-blue": "#E5EFFE",
      },
    },
  },
  plugins: [],
};
export default config;
