# Chat App

Simple Chat App is a real-time messaging web application built with Next.js and Firebase. This application allows users to communicate instantly over the internet. You can check the deployed project on [Chat_App](https://chat-app-soldev.vercel.app/)

## Features

- Real-time messaging: Send and receive messages instantly across the internet.

- User authentication: Securely sign in with Google or anonymously with a username.

- Online status: See who's online in real-time.

## Screenshot

![Sign in page](https://gitlab.com/pamungkasbagaskara/chat-app/-/wikis/uploads/56bb0151fc8c8782b0e6a15ceaa37c3b/telegram-cloud-photo-size-5-6260130682252673449-y.jpg)
![Chat room page](https://gitlab.com/pamungkasbagaskara/chat-app/-/wikis/uploads/8532b8c3347d2f7407d23305d1b4b993/telegram-cloud-photo-size-5-6260130682252673447-y.jpg)
![Sign in mobile](https://gitlab.com/pamungkasbagaskara/chat-app/-/wikis/uploads/65811df710860f55a22140e1e10984f7/telegram-cloud-photo-size-5-6260130682252673450-y.jpg)
![Chat room mobil](https://gitlab.com/pamungkasbagaskara/chat-app/-/wikis/uploads/aeb4716379ab3fb0359de0d8d3048b4e/telegram-cloud-photo-size-5-6260130682252673452-y.jpg)

## Technologies and Libraries Used

- [Next.js](https://nextjs.org/): A React framework for building JavaScript applications.
- [React](https://reactjs.org/): A JavaScript library for building user interfaces.
- [Firebase](https://firebase.google.com/): A platform developed by Google for creating mobile and web applications.
- [Sentry](https://sentry.io/welcome/): An error tracking tool that helps developers monitor and fix crashes in real time.
- [Awilix](https://github.com/jeffijoe/awilix): A powerful, production-ready dependency injection (DI) container for JavaScript/TypeScript.
- [Dexie.js](https://dexie.org/): A minimalistic wrapper for IndexedDB, used for client-side storage.
- [Sass](https://sass-lang.com/): A preprocessor scripting language that is interpreted or compiled into CSS.

### Development Libraries

- [Jest](https://jestjs.io/): A delightful JavaScript Testing Framework with a focus on simplicity.
- [Testing Library](https://testing-library.com/): A family of libraries built for testing JavaScript applications in a way that resembles the way your software is used.
- [ESLint](https://eslint.org/): A tool for identifying and reporting on patterns found in ECMAScript/JavaScript code.
- [PostCSS](https://postcss.org/): A tool for transforming CSS with JavaScript.
- [Tailwind CSS](https://tailwindcss.com/): A utility-first CSS framework for rapidly building custom user interfaces.
- [TypeScript](https://www.typescriptlang.org/): An open-source language which builds on JavaScript, by adding static type definitions.

## Getting Started

To get a local copy up and running, follow these steps:

1. Clone the repository:

```bash

git  clone  https://github.com/yourusername/simple-chat-app.git

cd  simple-chat-app

```

2.  Install the dependencies:

```bash
npm  install
```

3.  Create a new Firebase project and add your Firebase configuration to a `.env.local` file in the root of your project:

```bash
NEXT_PUBLIC_FIREBASE_API_KEY=your-api-key
NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN=your-auth-domain
NEXT_PUBLIC_FIREBASE_PROJECT_ID=your-project-id
NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET=your-storage-bucket
NEXT_PUBLIC_FIREBASE_MESSAGING_SENDER_ID=your-messaging-sender-id
NEXT_PUBLIC_FIREBASE_APP_ID=your-app-id
```

4.  Run the development server:

```bash
npm  run  dev
```

5. Open http://localhost:3000 with your browser to see the result.

## Perform Testing

Run npm run test to see if the test-case pass

```

npm run test

```

Run this command to see test coverage on the project

```

npm run test:cov

```

## Deploy

This project deployed using vercel.

You can check how to deploy directly from your react project to vercel from [here](https://vercel.com/guides/deploying-react-with-vercel)

## CICD

The CICD in this project configured in pipeline editor gitlab with goals of automatic deploy to vercel whenever code is merged to main branch.

All the cicd variable has to state in the gitlab before get called in the script.

Before deploy to vercel, the pipeline job has to pass 4 other stages first

1. Install Stage --> to install node modules

2. Testing Stage --> test the code with test case, if all the testing pass then it can continue to deploy

3. Sonar Test --> Test code quality using sonarcloud

4. Deploy Stage --> deploy to vercel

```mermaid

graph LR

A(Install) --> B(Testing) --> C(SonarCloud Scan) --> D(Deploy Vercel)

```
